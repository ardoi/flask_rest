from flask import Flask, render_template
from flask.ext.restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

shoppinglist = {
    0: {'name':'Milk','count':0},
    1: {'name':'Honey','count':1},
    2: {'name':'Eggs','count':6}

}

class ListItem(Resource):
    def get(self, item_id):
        return shoppinglist[item_id]
    def delete(self, item_id):
        del shoppinglist[item_id]
        return 'deleted {}'.format(item_id), 204

class List(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('name', type=str, required=True)
    parser.add_argument('count', type=int, required=True)

    def get(self):
        return shoppinglist

    def post(self):
        args = self.__class__.parser.parse_args()
        new_id=max(shoppinglist.keys())+1
        shoppinglist[new_id] = args
        return shoppinglist[new_id], 201



api.add_resource(List, '/items')
api.add_resource(ListItem, '/item/<int:item_id>')

@app.route("/")
def showList():
    #return "<h1>shopping list is</h1>"+ str(shoppinglist)
    ret = render_template('shopping4.html', items=shoppinglist)
    return ret

app.debug=True
app.run(port=5001)
