from flask import Flask, render_template

app = Flask(__name__)

shoppinglist = {
    0: {'name':'Milk','count':0},
    1: {'name':'Honey','count':1},
    2: {'name':'Eggs','count':6}

}

@app.route("/")
def showList():
    ret = render_template('shopping4.html', items=shoppinglist)
    return ret

app.debug=True
app.run(port=5001)
